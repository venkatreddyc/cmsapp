<!DOCTYPE html>

<html lang="en">

	<head>
		<meta charset="utf-8">
		<title>Contact Management System</title>

		<meta name="viewport" content="width=device-width, initial-scale=1.0">
		<meta name="description" content="">
		<meta name="author" content="">

		<!-- Import / Refer stylesheets here  -->
		<link href="css/bootstrap.css" rel="stylesheet" type="text/css">
		<link href="css/cms_style.css" rel="stylesheet" type="text/css" >
		<link href="css/bootstrap-responsive.css" rel="stylesheet">

		<script type="text/javascript">	
		
			function updateRecord()	{
				var ResumeID = document.getElementsByName('ContactID');
				var idval = ResumeID;
				for (var i = 0, length = ResumeID.length; i < length; i++) 	{
						if (ResumeID[i].checked) {
							idval = ResumeID[i].value;
						}
					}

				var url="http://toolsjunction.com/apps/cmsapp/updateRecord.php?id="+idval;
				window.open( url, "myWindow", "status = 1, height = 600, width = 550, resizable = 1" )
			}
				
			function deleteRecord(ResumeID) {
				var idval = ResumeID;
				
				var ResumeID = document.getElementsByName('ContactID');
				for (var i = 0, length = ResumeID.length; i < length; i++) 	{
					if (ResumeID[i].checked) {
						idval = ResumeID[i].value;
					}
				}
		
				var url="http://toolsjunction.com/apps/cmsapp/deleteRecord.php?id="+idval;
				window.open( url, "myWindow", "status = 1, height = 600, width = 550, resizable = 1" )
			}
			
			function viewDetails(ResumeID) {
				var ResumeID = document.getElementsByName('ContactID');
				var idval = ResumeID;
				for (var i = 0, length = ResumeID.length; i < length; i++) 	{
					if (ResumeID[i].checked) {
						idval = ResumeID[i].value;
					}
				}
			
				var url="http://toolsjunction.com/apps/cmsapp/viewDetails.php?id="+idval;
				window.open( url, "myWindow", "status = 1, height = 600, width = 550, resizable = 1" )
			}
			
			function addRecord() {
				var url="http://toolsjunction.com/apps/cmsapp/addRecord.php";
				window.open( url, "myWindow", "status = 1, height = 600, width = 550, resizable = 1" )
			}

			function uploadimpCSVFile() {
				var url="http://toolsjunction.com/apps/cmsapp/impCSVFile.php";
				window.open( url, "myWindow", "status = 1, height = 400, width = 400, resizable = 1" )
			}
			
			function searchContacts() 	{
				var url="http://toolsjunction.com/apps/cmsapp/searchRecords.php";
				window.open( url, "myWindow", "status = 1, height = 600, width = 550, resizable = 1" )
			}
			
		</script>
	</head>
	
	<body align="center">
		<div class="container">
			<div  align="center"><h3>Contact Management System</h3></div>
		</div>
		
		<div id="menu" align="center">
			<input type="button" name="HOME" class="btn-primary btn-success" value="HOME" onclick="window.location ='http://toolsjunction.com/apps/cmsapp/viewRecords.php';">
			<input type="button" name="ADD" class="btn-primary btn-success" value="ADD" onclick="addRecord()">
			<input type="button" name="UPDATE" class="btn-primary btn-success" value="UPDATE" onclick="updateRecord()">
			<input type="button" name="UPLOAD" class="btn-primary btn-success" value="UPLOAD" onclick="uploadimpCSVFile()">
			<input type="button" name="DELETE" class="btn-primary btn-success" value="DELETE" onclick="deleteRecord()">
			<input type="button" name="VIEW DETAILS" class="btn-primary btn-success" value="VIEWDETAILS" onclick="viewDetails()">
			<input type="button" name="EXPORT" class="btn-primary btn-success" value="EXPORT CONTACTS" onclick="window.location='export.php'">
		</div>
	
		<div align="right">
 		    <form method="post" action="http://toolsjunction.com/apps/cmsapp/searchRecords.php" id="searchform">
				<input name="NameoftheCandidate" placeholder="Search here" class="span2">
				<button type="submit" name="submit"  class="btn-mini btn-info"  value="Search">Search</button>		
			</form>
		</div>

	<?php
		error_reporting(~E_NOTICE);
		
		include "config.php";

	
		
		$sql="SELECT * FROM $tbl_name";
		$result=mysql_query($sql);
	?>

	<?php
		// this code is for page navigation
		$tbl_name="contacts";		//your table name

		// How many adjacent pages should be shown on each side?
		$adjacents = 3;

		/* 
		   First get total number of rows in data table. 
		   If you have a WHERE clause in your query, make sure you mirror it here.
		*/

		$query = "SELECT COUNT(*) as num FROM $tbl_name";
		$total_pages = mysql_fetch_array(mysql_query($query));
		$total_pages = $total_pages['num'];
			
		/* Setup vars for query. */
		$targetpage = "viewRecords.php"; 	//your file name  (the name of this file)
		$limit = 10; 								//how many items to show per page
		$page = $_GET['page'];
		if($page) 
			$start = ($page - 1) * $limit; 			//first item to display on this page
		else
			$start = 0;								//if no page var is given, set start to 0
			
		/* Get data. */
		$sql = "SELECT * FROM $tbl_name LIMIT $start, $limit";
		$result = mysql_query($sql);
			
		/* Setup page vars for display. */
		if ($page == 0) 
			$page = 1;					//if no page var is given, default to 1.
		$prev = $page - 1;							//previous page is page - 1
		$next = $page + 1;							//next page is page + 1
		$lastpage = ceil($total_pages/$limit);		//lastpage is = total pages / items per page, rounded up.
		$lpm1 = $lastpage - 1;						//last page minus 1
			
		/* 
			Now we apply our rules and draw the pagination object. 
			We're actually saving the code to a variable in case we want to draw it more than once.
		*/

		$pagination = "";
		if($lastpage > 1) 	{	
			$pagination .= "<div class=\"pagination\" >";
			//previous button
			if ($page > 1) 
				$pagination.= "<a href=\"$targetpage?page=$prev\"><< previous</a>";
			else
				$pagination.= "<span class=\"disabled\"><< previous</span>";	

			//pages	
			if ($lastpage < 7 + ($adjacents * 2)) {	//not enough pages to bother breaking it up
				for ($counter = 1; $counter <= $lastpage; $counter++) {
					if ($counter == $page)
						$pagination.= "<span class=\"current\">$counter</span>";
					else
							$pagination.= "<a href=\"$targetpage?page=$counter\">$counter</a>";					
				} // end of for loop
			} elseif($lastpage > 5 + ($adjacents * 2)) {	
				//enough pages to hide some //close to beginning; only hide later pages
				if($page < 1 + ($adjacents * 2)) {
					for ($counter = 1; $counter < 4 + ($adjacents * 2); $counter++)	{
							if ($counter == $page)
								$pagination.= "<span class=\"current\">$counter</span>";
							else
								$pagination.= "<a href=\"$targetpage?page=$counter\">$counter</a>";					
					} // end of for
					
					$pagination.= "...";
					$pagination.= "<a href=\"$targetpage?page=$lpm1\">$lpm1</a>";
					$pagination.= "<a href=\"$targetpage?page=$lastpage\">$lastpage</a>";		
				} //in middle; hide some front and some back
				elseif($lastpage - ($adjacents * 2) > $page && $page > ($adjacents * 2)) {

					$pagination.= "<a href=\"$targetpage?page=1\">1</a>";
					$pagination.= "<a href=\"$targetpage?page=2\">2</a>";
					$pagination.= "...";
						
					for ($counter = $page - $adjacents; $counter <= $page + $adjacents; $counter++)	{
						if ($counter == $page)
							$pagination.= "<span class=\"current\">$counter</span>";
						else
							$pagination.= "<a href=\"$targetpage?page=$counter\">$counter</a>";					
					} // end of for
					$pagination.= "...";
					$pagination.= "<a href=\"$targetpage?page=$lpm1\">$lpm1</a>";
					$pagination.= "<a href=\"$targetpage?page=$lastpage\">$lastpage</a>";		
				} // end of else if - close to end; only hide early pages
				else {
					$pagination.= "<a href=\"$targetpage?page=1\">1</a>";
					$pagination.= "<a href=\"$targetpage?page=2\">2</a>";
					$pagination.= "...";
					for ($counter = $lastpage - (2 + ($adjacents * 2)); $counter <= $lastpage; $counter++)	{
						if ($counter == $page)
							$pagination.= "<span class=\"current\">$counter</span>";
						else
							$pagination.= "<a href=\"$targetpage?page=$counter\">$counter</a>";					
					} // end of for loop
				} // end of else
			} // end of else if for lastpage > 5
				
			//next button
			if ($page < $counter - 1) 
				$pagination.= "<a href=\"$targetpage?page=$next\">next >></a>";
			else
				$pagination.= "<span class=\"disabled\">next >></span>";
				
			$pagination.= "</div>\n";		
		}	// end of if
		
	?>
		
		<div class="container" style="max-width:900px"> 
			<table align="center" border="1">
				<tr>
					<td>---</td>
					<td align="center">Name</td>
					<td align="center">Phone</td>
					<td align="center">Email</td>
					<td align="center">Location</td>
					<td align="center">Employer</td>
					<td align="center">Designation</td>
					
				</tr>

				<?php
					while($rows=mysql_fetch_array($result)){
				?>

				<tr>
					<td> <input type="radio" name="ContactID" value="<?echo $rows['ResumeID']; ?>" ></td>
					<td><? echo $rows['NameoftheCandidate'];  ?>  </td>
					<td><? echo $rows['TelephoneNo']; ?></td>
					<td><? echo $rows['Email']; ?></td>
					<td><? echo $rows['CurrentLocation']; ?></td>
					<td><? echo $rows['CurrentEmployer']; ?></td>
					<td><? echo $rows['CurrentDesignation']; ?></td>
					
				</tr>

				<?php
					} // end of while loop
				?>
			</table>
		
		 
		<?=$pagination?>
	
		</div>
		</br>
		
		</br>
		
	</body>

  </html>
